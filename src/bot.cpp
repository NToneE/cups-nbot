#include <stdio.h>
#include "bot.h"
#include <cups/cups.h>

TgBot::Bot Bot::makeTgBot() {
    config.setup();
    return TgBot::Bot(config.token);
}

bool Bot::userAuthorized(const TgBot::Message::Ptr &message) const {
    return message->from->id == config.admin_id;
}

TgBot::Message::Ptr Bot::reply(const TgBot::Message::Ptr &message, const std::string &text) const {
    return tg.getApi().sendMessage(message->chat->id, text);
}

int uploadFile(cups_dest_t *cups_dest, const std::string &filename, const std::string &mimetype,
                FILE *file) {
    static constexpr uint32_t kBufferSize = 1 << 16;
    // Upload file from stream to CUPS server. Returns 0 in case of error occurred.
    int32_t job_id = 0;
    cups_dinfo_t *cups_dest_info = cupsCopyDestInfo(CUPS_HTTP_DEFAULT, cups_dest);
    if (cupsCreateDestJob(CUPS_HTTP_DEFAULT, cups_dest, cups_dest_info,
                          &job_id, filename.c_str(), 0, nullptr) != IPP_OK) {
        fprintf(stderr, "error while creating job [%s]\n", cupsLastErrorString());
        return 0;
    }
    if (cupsStartDestDocument(CUPS_HTTP_DEFAULT, cups_dest, cups_dest_info, job_id,
                              filename.c_str(), mimetype.c_str(),
                              0, nullptr, 1) != HTTP_STATUS_CONTINUE) {
        fprintf(stderr, "error while starting document for job_id=%d [%s]\n", job_id, cupsLastErrorString());
        return 0;
    }
    char *buffer = new char[kBufferSize];
    uint32_t last_read;
    while ((last_read = fread(buffer, 1, kBufferSize, file)) > 0) {
        if (cupsWriteRequestData(CUPS_HTTP_DEFAULT, buffer, last_read) != HTTP_STATUS_CONTINUE) {
            fprintf(stderr, "error while uploading document for job_id=%d [%s]\n", job_id, cupsLastErrorString());
            return 0;
        }
        fprintf(stderr, "sent %u bytes of file\n", last_read);
    }
    if (cupsFinishDestDocument(CUPS_HTTP_DEFAULT, cups_dest, cups_dest_info) == IPP_STATUS_OK) {
        fprintf(stderr, "successfully finished file\n");
        return job_id;
    }
    else {
        fprintf(stderr, "error while finishing job=%d [%s]\n", job_id, cupsLastErrorString());
        return 0;
    }
}

void Bot::onStart(const TgBot::Message::Ptr &message) {
    if (!userAuthorized(message)) {
        return;
    }
    reply(message, "Привет!\n/plain — распечатать текст из сообщения,\n[файл] — распечатать файл");
}

void Bot::onCommandPlain(const TgBot::Message::Ptr &message) {
    if (!userAuthorized(message)) {
        return;
    }
    int32_t first_space = -1;
    for (int32_t i = 0; i < message->text.length(); ++i) {
        if (isspace(message->text[i])) {
            first_space = i;
            break;
        }
    }
    if (first_space == -1) {
        reply(message, "usage: /plain [какой-то текст]");
        return;
    }
    ++first_space;
    reply(message, "Сейчас попробую");
    int32_t job_id = 0;
    job_id = uploadFile(config.cups_destination, "telegram-message.txt", CUPS_FORMAT_TEXT,
                        fmemopen((void *) message->text.substr(first_space).c_str(),
                                 message->text.length() - first_space, "r"));
    if (job_id) {
        char job_id_buf[128];
        sprintf(job_id_buf, "✅ (job id: %d)", job_id);
        reply(message, job_id_buf);
    }
    else {
        reply(message, "Что-то пошло не так");
    }
}

bool checkMimeType(const std::string &mimetype) {
    return (mimetype == CUPS_FORMAT_JPEG || mimetype == CUPS_FORMAT_PDF ||
            mimetype == CUPS_FORMAT_TEXT || mimetype == CUPS_FORMAT_POSTSCRIPT);
}

void Bot::onAnyMessage(const TgBot::Message::Ptr &message) {
    constexpr static uint32_t kFileMaxSize = 20'000'000;  // Telegram's limit on public Bot API server
    if (!userAuthorized(message)) {
        return;
    }
    if (!message->document) {
        return;
    }
    if (!checkMimeType(message->document->mimeType)) {
        char messageRes[4096];  // maximum message size for telegram
        sprintf(messageRes,
                "Получен неподдерживаемый файл (mime-тип %s).\nПоддерживаются mime-типы %s, %s, %s, %s.",
                message->document->mimeType.c_str(), CUPS_FORMAT_JPEG, CUPS_FORMAT_PDF,
                CUPS_FORMAT_TEXT, CUPS_FORMAT_POSTSCRIPT);
        reply(message, messageRes);
        return;
    }
    if (message->document->fileSize > kFileMaxSize) {
        char messageRes[4096];
        sprintf(messageRes, "Получен слишком большой файл (%u байт, максимально разрешенный размер %u байт)",
                message->document->fileSize, kFileMaxSize);
        reply(message, messageRes);
        return;
    }
    std::string file = tg.getApi().downloadFile(tg.getApi().getFile(message->document->fileId)->filePath);
    reply(message, "Сейчас попробую");
    int job_id = uploadFile(config.cups_destination, message->document->fileName, message->document->mimeType,
                   fmemopen((void *)file.c_str(),file.length(), "r"));
    if (job_id) {
        char job_id_buf[128];
        sprintf(job_id_buf, "✅ (job id: %d)", job_id);
        reply(message, job_id_buf);
    }
    else {
        reply(message, "Что-то пошло не так");
    }
}

Bot::Bot(): tg(makeTgBot()) {
    tg.getEvents().onCommand("start",
                             [this](const TgBot::Message::Ptr &message) { onStart(message); });
    tg.getEvents().onAnyMessage([this](const TgBot::Message::Ptr &message) { onAnyMessage(message); });
    tg.getEvents().onCommand("plain", [this](const TgBot::Message::Ptr &message) { onCommandPlain(message); });
};