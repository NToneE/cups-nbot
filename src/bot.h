#ifndef CUPS_NBOT_BOT_H
#define CUPS_NBOT_BOT_H

#include <tgbot/tgbot.h>
#include "config.h"

struct Bot {
    BotConfig config;
    TgBot::Bot tg;

    Bot();
private:
    TgBot::Bot makeTgBot();
    bool userAuthorized(const TgBot::Message::Ptr& message) const;
    void onStart(const TgBot::Message::Ptr& message);
    void onCommandPlain(const TgBot::Message::Ptr &message);
    void onAnyMessage(const TgBot::Message::Ptr &message);
    TgBot::Message::Ptr reply(const TgBot::Message::Ptr& message, const std::string &text) const;
};

#endif //CUPS_NBOT_BOT_H
