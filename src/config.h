#ifndef CUPS_NBOT_CONFIG_H
#define CUPS_NBOT_CONFIG_H

#include <cstdint>
#include <cstdlib>
#include <stdexcept>
#include <string>
#include <cups/cups.h>

struct BotConfig {
    int64_t admin_id;
    cups_dest_t *cups_destination;
    std::string token;

    BotConfig();
    void setup();
private:
    constexpr static const char* kEnvVarTelegramToken = "CUPS_NBOT_TOKEN";
    constexpr static const char* kEnvVarAdminID = "CUPS_NBOT_ADMIN_ID";
    constexpr static const char* kEnvVarCUPSDestinationName = "CUPS_NBOT_DESTINATION_PRINTER";
};

#endif //CUPS_NBOT_CONFIG_H
