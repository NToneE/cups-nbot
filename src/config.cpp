#include <cstdio>
#include <string>
#include <functional>
#include "config.h"
#include <cups/cups.h>

void ensureVariableExists(const char* name) {
    char* got = getenv(name);
    if (!got) {
        char err[128];
        sprintf(err, "%s is empty", name);
        fprintf(stdout, "%s", err);
        throw std::runtime_error(err);
    }
}

int cupsDestinationSetterCallback(void *user_data, unsigned flags, cups_dest_t *dest) {
    char *data = (char*)(user_data) + sizeof(dest);
    auto result = *((cups_dest_t**)(user_data));

    if (strcmp(dest->name, data) == 0) {
        auto &ref = *result;
        ref = cups_dest_t(*dest);
        ref.name = strdup(ref.name);
        ref.options = new cups_option_t[ref.num_options];
        for (int i = 0; i < ref.num_options; ++i) {
            ref.options[i].name = strdup(dest->options[i].name);
            ref.options[i].value = strdup(dest->options[i].value);
        }
    }
    return (1);
}

cups_dest_t* setCupsDestination(const std::string &destination_name) {
    auto *ref = new cups_dest_t();
    char user_data_buf[256];
    ((cups_dest_t**)(user_data_buf))[0] = ref;
    memcpy(user_data_buf + sizeof(ref), destination_name.c_str(), destination_name.size());
    user_data_buf[sizeof(ref) + destination_name.size()] = '\0';
    cupsEnumDests(CUPS_DEST_FLAGS_NONE, 1000, nullptr, 0, 0,
                  cupsDestinationSetterCallback, (void*)user_data_buf);
    if (!ref->name) {
        throw std::runtime_error("failed to set cups dest");
    }
    else {
        return ref;
    }
}


void BotConfig::setup() {
    ensureVariableExists(kEnvVarTelegramToken);
    ensureVariableExists(kEnvVarAdminID);
    ensureVariableExists(kEnvVarCUPSDestinationName);
    char* token_cstr = strdup(getenv(kEnvVarTelegramToken));
    token = token_cstr;
    char* admin_id_env = getenv(kEnvVarAdminID);
    admin_id = strtoll(admin_id_env, nullptr, 0);
    if (admin_id <= 0) {
        throw std::runtime_error("bad admin ID provided; it must be an integer greater than zero");
    }
    std::string cups_destination_name = strdup(getenv(kEnvVarCUPSDestinationName));
    cups_destination = setCupsDestination(cups_destination_name);
}

BotConfig::BotConfig(): token(), admin_id(), cups_destination() {
    setup();
}
