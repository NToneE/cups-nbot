#include <csignal>
#include <cstdio>
#include <exception>
#include <tgbot/tgbot.h>
#include "bot.h"

#include <cups/cups.h>

void sigint_handler(int) {
    fprintf(stderr, "Got SIGINT, terminating");
    exit(EXIT_SUCCESS);
}

int main() {
    Bot bot;
    TgBot::TgLongPoll longpoll(bot.tg);

    fprintf(stderr, "Logged in as @%s\n", bot.tg.getApi().getMe()->username.c_str());

    signal(SIGINT, sigint_handler);

    while (true) {
//        fprintf(stderr, "Starting longpoll\n");
        try {
            longpoll.start();
        }
        catch (std::exception &e) {
            fprintf(stderr, "An error occured while processing updates:\n> %s\n", e.what());
        }
    }
}
